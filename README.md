#README#

**Authors:** *Aashita, Huu, Timothy*
## Intro ##
This program is meant to read sensor values from drum sensor (attached to the drum set) 
and display a music sheet of the note(s) played. There are two parts to the program: 
the User Interface and the Arduino.

## User Interface ##
The User Interface will be written in C# and utilize Windows Presentation Foundation (WPF) 
to display information. This is where all the notes played will appear in a music sheet format.

## Arduino ##
An [Arduino](https://www.arduino.cc/) will be used to recieve the signal from the [Piezo Sensors](https://www.sparkfun.com/tutorials/330). Upon receiving the 
signals, the Arduino will package and send the information to the main computer which will 
display the information. The Arduino itself will be responsible for the timing and packaging
the data.
