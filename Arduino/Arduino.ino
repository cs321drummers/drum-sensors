#define NUMSENSOR 8
#define HACK true

byte connect = 0;

typedef struct _SensorData {
	int port;
	int hit;
	int timer;
	int peekVal;
	int currentVal;
	int preValue;
	int thresh;
	const char *note;
} SensorData;

SensorData sData [ 8 ];
int num = 1; // hack
int timer = 0; // hack

void setup ( ) {
	Serial.begin ( 9600 );
	sData [ 0 ] = ( SensorData ) {
		.port = 0, .hit = 0, .timer = 0, .peekVal = 0, .currentVal = 0, .preValue = 0, .thresh = 900, .note = "Ride" 
	};
	sData [ 1 ] = ( SensorData ) {
		.port = 2, .hit = 0, .timer = 0, .peekVal = 0, .currentVal = 0, .preValue = 0, .thresh = 1000, .note = "Crash"
	};
	sData [ 2 ] = ( SensorData ) {
		.port = 4, .hit = 0, .timer = 0, .peekVal = 0, .currentVal = 0, .preValue = 0, .thresh = 100, .note = "High-hat"
	};
	sData [ 3 ] = ( SensorData ) {
		.port = 6, .hit = 0, .timer = 0, .peekVal = 0, .currentVal = 0, .preValue = 0, .thresh = 500, .note = "Snare"
	};
	sData [ 4 ] = ( SensorData ) {
		.port = 8, .hit = 0, .timer = 0, .peekVal = 0, .currentVal = 0, .preValue = 0, .thresh = 1000, .note = "High Tom"
	};
	sData [ 5 ] = ( SensorData ) {
		.port = 10, .hit = 0, .timer = 0, .peekVal = 0, .currentVal = 0, .preValue = 0, .thresh = 150, .note = "Bass" //700
	};
	sData [ 6 ] = ( SensorData ) {
		.port = 12, .hit = 0, .timer = 0, .peekVal = 0, .currentVal = 0, .preValue = 0, .thresh = 150, .note = "Floor Tom" //700
	};
	sData [ 7 ] = ( SensorData ) {
		.port = 14, .hit = 0, .timer = 0, .peekVal = 0, .currentVal = 0, .preValue = 0, .thresh = 150, .note = "Bass" //1000
	};

}

//Keep sending id information until we get a reply to start the Arduino
byte ConnecttoComputer ( ) {
	char receive [ 2 ] = { 0, 0 };
	byte byteArray [ 5 ] = { 64, 114, 100, 13, 10 }; // Binary for string "@rd\r\n"
	// send id signal
	Serial.write ( byteArray, 5 );
	delay ( 1 );
	// read id signal
	Serial.readBytes ( receive, 2 ); // response signal should create string "C#"
	if ( receive [ 0 ] == 67 && receive [ 1 ] == 35 )
		return 1; // if the id is correct, we will continue to the main loop
	return 0;
}

//run loop 8 times - calls readSensor pass in pointer of sData
//make
void loop ( ) {
	//initialize a new byte array with all 0s
	char exitSignalArray [ 3 ] = { 0, 0, 0 };
	//if the byte is 0 then call the function and set connect equal to the return value of the function
	if ( connect == 0 ) {
		connect = ConnecttoComputer ( );
	}
	//otherwise create a new char array
	if ( connect == 1 ) {
		char SensorByte [ 10 ] = { 0, 0, 0, 0, 0, 0, 0, 0, 13, 10 };
		if (HACK) {
			//go through the number of sensors that are present and initialize each index of the SensorByte
			for ( int i = 5; i < NUMSENSOR; i++ ) {
				SensorByte [ i ] = ReadSensor ( &sData [ i ] );
			}
		} else { // hack
			if (timer == 0)  { // make random notes every 500 cycle (about < half a second
				SensorByte[0] = num % 10 == 0? 1 :0;
				SensorByte[1] = num % 2 == 0? 1 :0;
				SensorByte[2] = num % 3 == 0? 1 :0;
				SensorByte[3] = num % 4 == 0? 1 :0;
				SensorByte[4] = num % 5 == 0? 1 :0;
				SensorByte[5] = num % 6 == 0? 1 :0;
				SensorByte[6] = num % 7 == 0? 1 :0;
				SensorByte[7] = num % 8 == 0? 1 :0;
				timer = 500;
			} else timer --;
		}
		Serial.write ( SensorByte, 10 );

		if ( Serial.available ( ) > 1 ) {
			Serial.readBytes ( exitSignalArray, 3 );
			connect = 0; //set connect back to 0
		}
	}
}


// Read the sensor value and try to detect a hit through checking peek values
int ReadSensor ( SensorData *sensor ) {
	sensor->hit = 0;
	// We want to wait at a few nanoseconds before trying to see if the user "hit" again
	if ( sensor->timer == 0 ) {
		sensor->currentVal = analogRead ( sensor->port );
		// only if the hit is stronger than the last peek do we count it as a hit
		if ( sensor->currentVal > sensor->thresh && sensor->currentVal >= sensor->peekVal ) {
			sensor->hit = 1;
			sensor->timer = 10; // allow waiting of approx 1/10 of a second before resume reading
			sensor->peekVal = sensor->currentVal;
			return 1; //if its a hit
		} else if ( sensor->currentVal < sensor->thresh ) {
			sensor->peekVal = 0;
		} else if ( sensor->currentVal > sensor->preValue ) {
			sensor->peekVal = sensor->currentVal;
		}
	} else { // count down timer
		sensor->timer--;
	}


	// record our previous sensor value
	sensor->preValue = sensor->currentVal;
	return 0; //if its not a hit
	/*
	   DEBUG
	 */
	/*
	   if (sensor->hit) {
	   Serial.print("Sensor Port: ");
	   Serial.print(sensor->port);
	   Serial.print(" Sensor ");
	   Serial.print( (sensor->hit == 1 ? "Hit" : "Nothing") );
	   Serial.print("\tTimer value is - ");
	   Serial.print(sensor->timer);
	   Serial.print("\tPeek - ");
	   Serial.print(sensor->peekVal);
	   Serial.print("\tcurrentVal - ");
	   Serial.print(sensor->currentVal);
	   Serial.print("\tpreValue - ");
	   Serial.println(sensor->preValue);
	   }
	 */
	/*
	   End Debug
	 */
}
