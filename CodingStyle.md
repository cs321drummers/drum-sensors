Coding Style
	1) Use Tab (if you must use spaces, put 4 space)
	2) Brackets goes on the block header
		EX:
			void loop () { // bracket goes here
				if (1 > 4) { // bracket goes here
					...
				}
				...
			}
	3) Name variables for what they stand for, but try not 
		to make the name longer than about 10 characters long.
		It gets tedious writing long names and is bound 
		to cause mistakes.
	4) Ensure that variables names are remotely distinctive.
		EX:
			int sensorValue;
			int SensorValue;		// I will report you if you do this
			int sensorValues[10];	// DO NOT DO THIS
			int senValArray[10;		// This is fine
	5) Variable names should be camelCase. That means first letter is always
		lowercase. The any other words or important letters should start
		capital
			EX:
				int sensorValue;	// "V" is capital because it start a new word
				int isSenHit;		// "S" and "H" is capital because they start a new word
				int cSV;			// This stands for "current sensor value" (a made up abbreviation)
									// so "S" and "V" is capital because they are "important" letters
									// in the abbreviation
	6**) Methods name should be pascal case, meaning every word or important 
		character is capital. Note that the default functions (loop and setup) are
		required to be lower case. 
			EX:
				void ReadSensor () { } 
	7) No more than 4 nested blocks (really it should be 3 but...). Blocks 
		includes if, for, switch, etc...
		If you need more than 4 for some reason, you are probably unoptimized and
		need to fix the logic. If there REALLY isn't any better means, then split the
		blocks into another function and call that function instead
	8) Functions should really not be longer than 100 lines (minus all comments). Aim
		for no longer than 50 - 75, but can surpass if needed (up to 100). If you need
		more than 100 lines, the function can 100% be split into two or more specialized
		functions. So split it and call other functions
	9) Document what the function's functionality. Does not need to be long with an exception.
		Unless you want the world to burn, do your utmost to document more complex and longer
		functions. 
			EX:
				// This function reads the sensor value
				void ReadSensor () {...}
	10) Write self documenting code so you don't need to document code often
	11) Document your code if needed. If you think that the block of code is a bit
		complex, or want to ensure what it is understandable, gently document it
		Don't document obvious code please. We are college students. Let your
		code tell the story.
			EX:
				int value = analogRead(10)	// store sensor value into the variable value
											// Do not do the above (^). It is redundant
	12) Break up long lines (this is easily if your variable are not miles long). 
		Aim to keep each line about 100 or less columns long. Easily way if you don't
		have a column counter is to measure your line against other lines of code. If
		your line is longer than the longest line already in the code, then it is probably
		too long. This helps extremely with having side by side windows. 
		Your editor is one side and a webpage (API) is on the other. With line that are
		less than 100 columns long, there will not (most of the time) be an random line
		wrap which looks ugly and hard to navigate.
	13) Debugging is great. Add debugging section in the function as needed, but make sure to
	mark the section as debugging.
		EX:
			void ReadSensor () {
				...
				/*
				* Debugging 
				*/
				Serial.println("debug message here");
				...
				/*
				* End Debugging
				*/
			}
	
Git Commit Messages
	As yall probably will see or already know, git commit message only allow so long
	of a message. So we should probably use some abbreviations
		
		"+" -> Means add something
			EX:
				+ReadSensor		# This reads to I add the ReadSensor Function and it is working
		"-" -> Means remove something needs to go with ":"
			EX:
				-ReadSensor: usles fuct		# This reads to I removed ReadSensor because it is 
											# a useless function
		":" -> Means because or explaining something
		"&" -> Means and
		"|" -> Means or (This character is below the backspace key)
		"fuct" -> Means function
		"var" -> Means variable
		Add if needed

	You can make abbreviations as need but make them obvious if possible. For more complex
	and more often used abbreviation, you should probably add it to this list

	Sometime, we do not always have an Arduino at hand or Visual Studio or something, but
	have a great idea we think can work. In this case, what you should do is write up the code
	in an editor (any editor) and try to ensure to the best of your ability to that it is valid code.
	Then mark the block of code that you need to test or validate with a comment
		// TEST {initial}{current test #}
	Ensure that at least the "TEST" is all caps as it marks a tag

	Afterward, commit the changes with "{Message} (NT {first initial}{current # of tests})". What this
	is saying in the message is
		{Message}		- This is what I changed
		(NT ...			- Needs Testing
		{first initial}	- So we can find you if you try to do something funny (there are other ways
						to look you up, but responsibility
		{current #...}	- What test number are you at? This number represent how many open tests
						you current have open (open test are those that we are still debugging)
	Then send a GroupMe Message about the test you want and if possible one of us can test it
	at home or if not possible, we will test it at our meeting time. Now once the test is complete,
	The test or confirmer should remove the comment marking the testing block and recommit with
		"{Message} (T {first initial}{test #})"
	With the initial and test number corresponding to the test just completed.
	
		EX:
			"+ReadSensor to detect hits (NT H1)" <-- This is the need test message
			"*ReadSensor to detect hits (T H1)" <-- This is the finish testing message

	This will help us keep track of debugging and addition to the code. Only do this
	if your code greatly affect the system and you are not sure if it works are not.
