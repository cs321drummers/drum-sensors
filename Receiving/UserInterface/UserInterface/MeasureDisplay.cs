﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

using NoteProcesser;

namespace UserInterface {
    public partial class MainWindow {
        /// <summary>
        /// Get the noted processed and dispaly it onscreen and store it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void DisplayNote ( object sender, List<(int, string)> e ) {
#if LOG
            Log ( "Starting DisplayNote" );
            Log ( "Current state is -> " + state );
#endif
            if ( state.Equals ( State.WAITING ) || state.Equals ( State.RECORDING ) || state.Equals ( State.OVERRIDE ) ) {
#if LOG
                Log ( "\tState allow displaying notes" );
#endif
                Vector currentPos = noteLinePos;
                int index = measureList.Count - 1;
#if LOG
                Log ( "\tCurrent NoteLinePost X -> " + noteLinePos.X + " | Y -> " + noteLinePos.Y );
                Log ( "\tCurrent measure's index -> " + index );
                Log ( "\tNote List Count -> " + e.Count );
#endif

                for ( int i = 0; i < e.Count; i++ ) {
#if LOG
                    Log ( "-- \t\tIndex -> " + i + " | Note Name -> " + e [ i ].noteName + " | Note Type -> " + e [ i ].noteType );
#endif
                    Vector pos = new Vector ( currentPos.X-noteLineMove, 0 );
                    /*
                     * 88       - Top Line (not a note)
                     * 92.5     - Ride + Crash Cymbal
                     * 96.5       - Hi-hat
                     * 101.5    - High-tom
                     * 106.5    - Middle-tom
                     * 111.5    - Snare
                     * 120.5    - Floor-tom
                     * 130.5    - Bass Drum
                     */
                    switch ( e [ i ].Item1 ) {
                        case 0: // Ride
                            pos.Y = currentPos.Y - 17.5;
                            break;
                        case 1:  // Crash
                            pos.Y = currentPos.Y - 17.5;
                            break;
                        case 2:  // Hi-Hat
                            pos.Y = currentPos.Y - 13.5;
                            break;
                        case 3: // High-Tom
                            pos.Y = currentPos.Y - 8.5;
                            break;
                        case 4:  // Middle-Tom
                            pos.Y = currentPos.Y - 3.5;
                            break;
                        case 5:  // Snare
                            pos.Y = currentPos.Y + 1.5;
                            break;
                        case 6:  // Floor-Tom
                            pos.Y = currentPos.Y + 10.5;
                            break;
                        case 7:  // Bass Drum
                            pos.Y = currentPos.Y + 20.5;
                            break;
                    }
#if LOG
                    Log ( "\t\t\tNote Added to Measure" );
                    Log ( "\t\t\tNote Postion is X -> " + e [ i ].pos.X + " | Y -> " + e [ i ].pos.Y );
#endif
                    Dispatcher.Invoke ( ( ) => {
                        Image note = new Image ( ) {
                            Source = PICTURELIST.Single ( z => z.UriSource.ToString ( ).Contains ( e [ i ].Item2 ) ),
                            RenderSize = new Size ( 12, 30 ),
                            Name = e [ i ].Item2
                        };

                        noteCanvas.Set ( note, pos );
                        noteCanvas.Children.Add ( note );

                        measureList [ index ].AddNote ( (note, pos) );
                    } );
#if LOG
                    Log ( "\t\t\tNote Added to Measure..." );
#endif
                }
                state = State.RECORDING;
#if LOG
                Log ( "\tFinish adding Notes..." );
#endif
            }
#if LOG
            Log ( "Current state is -> " + state );
            Log ( "Exiting DisplayNote..." );
#endif
        }

        /// <summary>
        /// Move the Note Line based on a timer
        /// </summary>
        /// <param name="sender"></param>
        internal void MoveNoteLine ( object sender ) {
#if LOG
            Log ( "Starting MoveNoteLine" );
            Log ( "Current state is -> " + state );
#endif
            if ( state.Equals ( State.RECORDING ) || state.Equals ( State.OVERRIDE ) ) {
#if LOG
                Log ( "\tNoteLine Allowed to move" );
#endif
                if ( noteLinePos.X >= 970 ) {
#if LOG
                    Log ( "\t\tNoteLine reach end of line (x = 970)" );
#endif
                    if ( noteLinePos.Y == 745 ) {
#if LOG
                        Log ( "\t\t\tNoteline at last line (y = 745)" );
#endif
                        noteLinePos.Y = 110;
                        measureList.Add ( new Measure ( ) );
                        Dispatcher.Invoke ( ( ) => {
                            LPageNum.Content = "Page " + ++pageNum;
                            noteCanvas.Children.Clear ( );
                        } );
#if LOG
                        Log ( "\t\t\tFinish adding new measure and change page number to -> " + pageNum );
                        Log ( "\t\t\tNoteline reset to top (y = 110)" );
#endif
                    } else {
                        noteLinePos.Y += 127;
#if LOG
                        Log ( "\t\tMove NoteLine down by one (y += 127)" );
#endif
                    }
                    noteLinePos.X = 10;
                }
#if LOG
                Log ( "Current NoteLine Position (before X Move) x -> " + noteLinePos.X + " | y -> " + noteLinePos.Y );
#endif
                Dispatcher.Invoke ( ( ) => {
                    noteLinePos.X += noteLineMove;
                    canvas.Set ( noteLineImage, noteLinePos );
                } );
#if LOG
                Log ( "New NoteLine Position (after X Move) x -> " + noteLinePos.X + " | y -> " + noteLinePos.Y );
#endif
            }
#if LOG
            Log ( "Exiting MoveNoteLine..." );
#endif
        }

        /// <summary>
        /// Start/Stop recording
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void StartStop ( object sender, RoutedEventArgs e ) {
#if LOG
            Log ( "Starting StartStop" );
#endif
            if ( state.Equals ( State.WAITING ) || state.Equals ( State.RECORDING ) ) {
#if LOG
                Log ( "\tSetting state to PAUSE" );
#endif
                state = State.PAUSE;
                BStart.Content = "Start Recording";
                noteLineTimer.Change ( Timeout.Infinite, 500 );
            } else if ( !state.Equals ( State.DISCONNECT ) && !state.Equals ( State.OVERRIDE ) ) {
#if LOG
                Log ( "\tSetting state to WAITING" );
#endif
                state = State.WAITING;
                BStart.Content = "Stop Recording";
                noteLineTimer.Change ( 10, 10 );
            }
#if LOG
            Log ( "Current state is -> " + state );
            Log ( "Exiting Start Stop..." );
#endif
        }

        /// <summary>
        /// Go back one page if possible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void PreviousPage ( object sender, RoutedEventArgs e ) {
            if ( pageNum == 1 ) // we are on the first page
                return;
            if ( state.Equals ( State.CONNECT ) || state.Equals ( State.DISCONNECT ) || state.Equals ( State.PAUSE ) ) {
                LPageNum.Content = "Page " + --pageNum;
                noteCanvas.Children.Clear ( );
                for ( int i = 0; i < measureList [ pageNum - 1 ].size; i++ ) {
                    noteCanvas.Children.Add ( measureList [ pageNum - 1 ] [ i ].Item1 );
                }
            }
        }
        /// <summary>
        /// Go forward one page if possible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void NextPage ( object sender, RoutedEventArgs e ) {
            if ( pageNum == measureList.Count ) // we are on the last page
                return;
            if ( state.Equals ( State.CONNECT ) || state.Equals ( State.DISCONNECT ) || state.Equals ( State.PAUSE ) ) {
                LPageNum.Content = "Page " + ++pageNum;
                noteCanvas.Children.Clear ( );
                for ( int i = 0; i < measureList [ pageNum - 1 ].size; i++ ) {
                    noteCanvas.Set ( measureList [ pageNum - 1 ] [ i ].Item1, measureList [ pageNum - 1 ] [ i ].Item2 );
                    noteCanvas.Children.Add ( measureList [ pageNum - 1 ] [ i ].Item1 );
                }
            }
        }

    }
}