﻿#define LOG
#undef LOG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

using NoteProcesser;

namespace UserInterface {
#if LOG
    public delegate void Logging ( string text );
#endif

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : IDisposable {
        public static List<BitmapImage> PICTURELIST;

        internal Processer processer;
        internal Thread reconnectThread;

        internal List<Measure> measureList;
        internal Timer noteLineTimer;
        internal Image noteLineImage;
        internal Vector noteLinePos;

        internal int pageNum;
        internal double noteLineMove;
        public State state;

#if LOG
        public static event Logging Log;
#endif

        public MainWindow ( ) {
            InitializeComponent ( );

            new Logger ( );
#if LOG
            Log ( "Starting up MainWindow" );
            Log ( "Establishing Close Event and State" );
#endif

            // We are not connected nor recording
            Closing += DisposeElement;
            state = State.DISCONNECT;

#if LOG
            Log ( "Starting InitializePictures" );
#endif
            // Get and store all images
            InitializePictures ( );

            // initialize the variables
            pageNum = 1;
            noteLineMove = 2;
            measureList = new List<Measure> ( ) { new Measure ( ) };
            processer = new Processer ( );
            processer.errorReceived += ErrorReceived;
            processer.processedNote += DisplayNote;
#if LOG
            Log ( "Connected Processer's errorReceived and processNote event" );
#endif

            //determine if the Arduion is connected
            if ( processer.isOpen ) {
                LStatusLine.Content = "Connected to Arduino";
                state = State.CONNECT;
#if LOG
                Log ( "Current State is -> " + state );
#endif
            } else { // else we want to run a background task that tries to connect
#if LOG
                Log ( "Arduino is not connected... Starting reconnect thread" );
#endif
                reconnectThread = new Thread ( Reconnect ) { IsBackground = true };
                reconnectThread.Start ( );
#if LOG
                Log ( "Reconnect Thread Started and Running" );
#endif
            }

#if LOG
            Log ( "Setting up NoteLine" );
#endif
            // initialize the noteLine Timer, Image and position
            noteLineTimer = new Timer ( new TimerCallback ( MoveNoteLine ),
                null, Timeout.Infinite, 500 );
            noteLineImage = new Image ( ) {
                Source = PICTURELIST.Single ( z =>
                    z.UriSource.ToString ( ).Contains ( "noteLine" ) ),
                RenderSize = new Size ( 6, 65 ), Name = "noteLine",
            };
            noteLinePos = new Vector ( 10, 110 );
#if LOG
            Log ( "NoteLine objects created" );
#endif
            // set the location and add the noteLine to the Canvas
            canvas.Set ( noteLineImage, noteLinePos );
            canvas.Children.Add ( noteLineImage );
#if LOG
            Log ( "Added NoteLine to Canvas at position -> " + noteLinePos.X + ", " + noteLinePos.Y );
#endif
        }

        /// <summary>
        /// Load all the images of the noteLine and Notes
        /// </summary>
        static internal void InitializePictures ( ) {
            PICTURELIST = new List<BitmapImage> ( );
#if LOG
            Log ( "\tFinding pictures..." );
#endif
            foreach ( String s in Directory.GetFiles ( "images/" ) ) {
#if LOG
                Log ( "\t\tFound" + s );
#endif
                BitmapImage b = new BitmapImage ( );
                b.BeginInit ( );
                b.CacheOption = BitmapCacheOption.OnLoad;
                b.UriSource = new Uri ( s, UriKind.Relative );
                b.EndInit ( );
                PICTURELIST.Add ( b );
#if LOG
                Log ( "\t\t\t" + s + " Added to list" );
#endif
            }
#if LOG
            Log ( "\tExiting InitializePictures" );
#endif
        }

        /// <summary>
        /// If we received an error in the Arduion connection, try reconnecting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void ErrorReceived ( object sender ) {
            state = State.DISCONNECT;
#if LOG
            Log ( "ERROR: Processer sent an error" );
            Log ( "ERROR: Current state is -> " + state );
#endif
            reconnectThread = new Thread ( Reconnect ) { IsBackground = true };
            reconnectThread.Start ( );
#if LOG
            Log ( "Reconnect Thread Started and Running" );
#endif
        }

        /// <summary>
        /// This is run in the background to try to reconnect to the Arduino
        /// when needed
        /// </summary>
        internal void Reconnect ( ) {
#if LOG
            Log ( "Reconnect Thread Established..." );
#endif
            string dots = "";
            int count = 0;
            while ( !processer.isOpen ) {
#if LOG
                Log ( "-- \t\tProcesser state is -> " + processer.isOpen );
#endif
                // Just some text animation
                switch ( count % 3 ) {
                    case 0:
                        dots = "."; break;
                    case 1:
                        dots = ".."; break;
                    case 2:
                        dots = "..."; break;
                }
                count++;
                Dispatcher.Invoke ( ( ) =>
                    LStatusLine.Content = "Error Connecting to Arduino - Retrying" + dots );
#if LOG
                Log ( "-- \t\tMaking new Processer" );
#endif
                processer = new Processer ( );
#if LOG
                Log ( "-- \t\tNew Processer Created" );
                Log ( "-- \t\tSleeping..." );
#endif
                Thread.Sleep ( 250 );
            }

            Dispatcher.Invoke ( ( ) => LStatusLine.Content = "Connected to Ardunio" );
            state = State.CONNECT;
#if LOG
            Log ( "-- \t\tConnected to Arduino" );
            Log ( "-- \t\tCurrent State is -> " + state );
#endif
        }

        /// <summary>
        /// Dispose and clean up with the window is closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void DisposeElement ( object sender, System.ComponentModel.CancelEventArgs e ) {
#if LOG
            Log ( "Exiting Program..." );
#endif
            Dispose ( );
        }

        public void Dispose ( ) {
#if LOG
            Log ( "Disposing MainWindow..." );
#endif
            Dispose ( true );
            GC.SuppressFinalize ( this );
        }

        protected virtual void Dispose ( bool dispose ) {
            if ( dispose ) {
#if LOG
                Log ( "Disposing Reconnect Thread..." );
#endif
                reconnectThread?.Abort ( );
#if LOG
                Log ( "Disposing noteLineTimer..." );
#endif
                noteLineTimer.Dispose ( );
                processer.Dispose ( );
                Logger.Dispose ( );
            }
        }
    }

    /// <summary>
    /// Hold the state of the Program
    /// Disconnect  - No Arduino found
    /// Connect     - We are connected to the Arduino
    /// Waiting     - We are waiting for a note hit (We establish that we want to record)
    /// Recording   - We are recording, noteLine is moving and displaying notes
    /// Pause       - We paused the recording
    /// Override    - Override all the state and manually display notes
    /// </summary>
    public enum State { DISCONNECT, CONNECT, WAITING, RECORDING, PAUSE, OVERRIDE }

    public static class Extension {
        public static void Set ( this Canvas can, UIElement con, Vector pos ) {
            Canvas.SetTop ( con, pos.Y );
            Canvas.SetLeft ( con, pos.X );
        }
    }
}