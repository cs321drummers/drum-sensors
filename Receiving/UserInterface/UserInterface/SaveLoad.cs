﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Controls;
using System.Linq;
using NoteProcesser;
using System.Windows;

namespace UserInterface {
    public partial class MainWindow {

        /// <summary>
        /// Save the current session in a file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void SaveSession ( object sender, RoutedEventArgs e ) {
            Microsoft.Win32.SaveFileDialog box = new Microsoft.Win32.SaveFileDialog ( ) {
                Title = "Save Session",
                DefaultExt = ".drum",
                Filter = "Drum Sheet (*.drum)|*.drum"
            };
            bool? result = box.ShowDialog ( );

            if ( result == true ) {
                SaveSession ( box.FileName, measureList );
            }
        }
        /// <summary>
        /// Load a session based on a file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void LoadSession ( object sender, RoutedEventArgs e ) {
            Microsoft.Win32.OpenFileDialog box = new Microsoft.Win32.OpenFileDialog ( ) {
                Title = "Load Session",
                DefaultExt = ".drum",
                Filter = "Drum Sheet (*.drum)|*.drum"
            };
            bool? result = box.ShowDialog ( );

            if ( result == true ) {
                LoadSession ( box.FileName, ref measureList );
                Dispatcher.Invoke ( ( ) => {
                    noteCanvas.Children.Clear ( );
                    for ( int i = 0; i < measureList [ 0 ].size; i++ ) {
                        noteCanvas.Set ( measureList [ 0 ] [ i ].Item1, measureList [ 0 ] [ i ].Item2 );
                        noteCanvas.Children.Add ( measureList [ 0 ] [ i ].Item1 );
                    }
                } );
            }
        }

        /// <summary>
        /// Save the current session into a .drum file
        /// </summary>
        /// <param name="location">Where to save the file</param>
        /// <param name="measure">The list of measure to save"</param>
        /// <returns></returns>
        public static bool SaveSession ( string location, List<Measure> measure ) {
            try {
                using ( BinaryWriter write = new BinaryWriter ( File.Open ( location, FileMode.Create ) ) ) {
                    write.Write ( "DRUM" ); // Label the file
                    write.Write ( new byte [ ] { 0, 0, 0, 0 } ); // buffer space
                    foreach ( Measure m in measure ) {
                        write.Write ( ( byte ) 0 ); // need new measure
                        for ( int i = 0; i < m.size; i++ ) {
                            write.Write ( ( byte ) 1 ); // this is a note
                            write.Write ( m [ i ].Item1.Name );
                            write.Write ( m [ i ].Item2.X );
                            write.Write ( m [ i ].Item2.Y );
                            write.Write ( ( byte ) 255 ); // end of note
                        }
                    }
                }
                return true;
            } catch ( Exception ) { return false; }
        }

        /// <summary>
        /// Load a session from a .drum file
        /// </summary>
        /// <param name="location">The location of the save file</param>
        /// <param name="measure">The list of measure to populate</param>
        /// <returns></returns>
        public static bool LoadSession ( string location, ref List<Measure> measure ) {
            try {
                measure = new List<Measure> ( ) { };
                int index = -1;

                using ( BinaryReader read = new BinaryReader ( File.Open ( location, FileMode.Open ) ) ) {
                    read.BaseStream.Seek ( 1, SeekOrigin.Begin );
                    char [ ] header = new char [ 4 ];

                    read.Read ( header, 0, 4 ); // Check the header
                    if ( Enumerable.SequenceEqual ( header, new char [ 4 ] { 'D', 'R', 'U', 'M' } ) ) {

                        read.BaseStream.Seek ( 4, SeekOrigin.Current );

                        while ( read.BaseStream.Position < read.BaseStream.Length - 1 ) { // keep reading until we get to the end
                            byte check = read.ReadByte ( );
                            if ( check == 1 ) { // This is a note block
                                //int count = read.ReadInt32 ( ); // how long is the type name
                                string type = read.ReadString ( ); // get type name

                                Image note = new Image ( ) { // make a new Image
                                    Source = PICTURELIST.Single ( z => z.UriSource.ToString ( ).Contains ( type ) ),
                                    RenderSize = new Size ( 12, 30 ),
                                    Name = type,
                                };

                                // Get the location of the note
                                Vector pos = new Vector ( read.ReadDouble ( ), read.ReadDouble ( ) );
                                measure [ index ].AddNote ( (note, pos) );

                                // Check to make sure we are not dealing with a corrupted file
                                if ( read.ReadByte ( ) != 255 )
                                    throw new Exception ( "Corrupted File!" );
                            } else if ( check == 0 ) { // if it is a 0, we need to add a new measure
                                index++;
                                measure.Add ( new Measure ( ) );
                            }
                        }
                    }
                }
            } catch ( Exception e ) {
                measure = new List<Measure> ( ) { new Measure ( ) }; return false;
            }
            return true;
        }
    }
}
