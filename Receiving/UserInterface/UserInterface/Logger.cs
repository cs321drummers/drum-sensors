﻿#define LOG
#undef LOG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using NoteProcesser;

namespace UserInterface {
    public class Logger {
        private static StreamWriter log;
        public static bool testing = false;

        public Logger () {
#if LOG
            MainWindow.Log += Log;
            Processer.Log += Log;
            Log ( "" );
#endif
        }

        private static void Log ( string text = "") {
            if ( !testing ) {
                if ( log == null ) {
                    Directory.CreateDirectory ( @"./Logs" );
                    //log = new StreamWriter ( File.Create ( ( @"./Logs/" + DateTime.Now.ToShortTimeString() ) ) );
                    log = new StreamWriter ( File.Create ( ( @"./Logs/" + DateTime.Now.ToString("MMMM dd, yyyy - HH mm ss tt") ) ) );
                }
                String textlog = "" + DateTime.Now + " -- " + text + "\n";
                log.Write ( textlog );
            }
        }

        public static void Dispose ( ) {
            Log ( "Program Successfully Exited..." );
            log.Dispose ( );
        }
    }
}
