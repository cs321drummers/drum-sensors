﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace UserInterface {
    public partial class MainWindow {
        #region DEBUGGING
#if DEBUG
        // Mouse Position 
        int click = 0;
        internal void DebugMouseMove ( object sender, MouseEventArgs e ) => LDebugMousePos.Content = e.GetPosition ( window );
        internal void DebugMouseClick ( object sender, MouseButtonEventArgs e ) {
            switch ( click ) {
                case 0:
                    LDebugMouseClick.Content = "1) " + e.GetPosition ( window );
                    click++; break;
                case 1:
                    LDebugMouseClick.Content += " || 2) " + e.GetPosition ( window );
                    click = 0; break;
            }
        }

        // Fake notes
        bool overRide = false;
        Random rand;
        ListBox drumList;
        private void OverrideNotes ( object sender, RoutedEventArgs e ) {
            if ( drumList == null )
                SetDrum ( );
            if ( !overRide ) {
                rand = new Random ( 0 );
                state = State.OVERRIDE;
                reconnectThread?.Abort ( );
                LStatusLine.Content = "OVERRIDED. RANDOM NOTES";
                noteLineTimer.Change ( 25, 25 );
                drumList.Visibility = Visibility.Visible;
                overRide = true;
            } else {
                state = State.DISCONNECT;
                reconnectThread = new Thread ( Reconnect ) { IsBackground = true };
                reconnectThread.Start ( );
                noteLineTimer.Change ( Timeout.Infinite, 500 );
                drumList.Visibility = Visibility.Hidden;
                overRide = false;

            }
        }

        private void SetDrum ( ) {
            drumList = new ListBox ( ) {
                ItemsSource = new List<string> ( ) {
                    "Ride Cymbal", "Crash Cymbal", "Hi-Hat",
                    "High-Tom", "Middle-Tom", "Snare", "Floor-Tom",
                    "Bass Drum" }
            };
            drumList.MouseDoubleClick += DrumListMouseDown;
            Canvas.SetBottom ( drumList, 0 );
            Canvas.SetRight ( drumList, 200 );
            canvas.Children.Add ( drumList );
        }

        private void DrumListMouseDown ( object sender, MouseButtonEventArgs e ) =>
            DisplayNote ( null, new List<(int, string)> ( ) { (drumList.SelectedIndex, "quarter") } );
#endif
        #endregion DEUBBGING
    }
}
