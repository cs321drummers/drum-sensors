﻿using System;
using System.IO.Ports;
using System.Collections.Generic;
using NoteProcesser;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting {
    [TestClass]
    public class ProcesserTests {
		[TestMethod]
		[ExpectedException ( typeof ( ArgumentException ) )]
		public void TestProcesser ( ) {
			Processer p = new Processer ( "NOCOM" );
		}

		[TestMethod]
		public void TestProcesserConstructor ( ) {
			if ( SerialPort.GetPortNames ( ).Length == 0 ) {
				Processer p = new Processer ( );
                Assert.IsFalse ( p.isOpen );
                Assert.IsFalse ( p.FindArduino ( ) );
                Assert.IsFalse ( p.FindArduino ("COM5" ) );
			} else
				Assert.Inconclusive ( "There are avialbable port open" );
		}

        [TestMethod]
        public void TestProcessDataEvent ( ) {
            Processer p = new Processer ( );
            byte [ ] b = new byte [ 11 ] { 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0 };

            p.processedNote += delegate ( object sender, List<(int, string)> e ) {
                Assert.AreEqual ( 4, e.Count );
                Assert.AreEqual ( ( 1, "quarter" ), e [ 0 ] );
                Assert.AreEqual ( ( 9, "quarter" ), e [ 0 ] );
                Assert.AreEqual ( ( 5, "quarter" ), e [ 0 ] );
                Assert.AreEqual ( ( 7, "quarter" ), e [ 0 ] );
            };
        }
    }
}
