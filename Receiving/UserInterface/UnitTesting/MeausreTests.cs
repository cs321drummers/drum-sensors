﻿using System;
using NoteProcesser;
using System.Windows;
using System.Windows.Controls;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting {
	[TestClass]
	public class MeausreTests {
		[TestMethod]
		[ExpectedException ( typeof ( ArgumentOutOfRangeException ) )]
		public void TestMeasureIndexingError_1 ( ) {
			Measure m = new Measure ( );
			(Image, Vector) e = m [ 0 ];
		}
		[TestMethod]
		[ExpectedException ( typeof ( IndexOutOfRangeException ) )]
		public void TestMeasureIndexingError_2 ( ) {
			Measure m = new Measure ( );
			(Image, Vector) e = m [ "something" ];
		}

		[TestMethod]
		public void TestMeasureConstructor ( ) {
			Measure m = new Measure ( );
			Assert.AreEqual ( 0 , m.size );
		}

		[TestMethod]
		public void TestMeasureIndexing ( ) {
			Measure m = new Measure ( );
			(Image, Vector) n1 = ( new Image ( ) { Name = "quarter" }, new Vector() );
			(Image, Vector) n2 = ( new Image ( ) { Name = "half" }, new Vector() );
			(Image, Vector) n3 = ( new Image ( ) { Name = "eighth" }, new Vector() );
			(Image, Vector) n4 = ( new Image ( ) { Name = "whole" }, new Vector() );

			m.AddNote ( n1 );
			m.AddNote ( n2 );
			m.AddNote ( n3 );
			m.AddNote ( n4 );

			Assert.AreEqual ( 4 , m.size );

			Assert.AreEqual ( n1 , m [ "first" ] );
			Assert.AreEqual ( n4 , m [ "last" ] );

			Assert.AreEqual ( n1 , m [ 0 ] );
			Assert.AreEqual ( n2 , m [ 1 ] );
			Assert.AreEqual ( n3 , m [ 2 ] );
			Assert.AreEqual ( n4 , m [ 3 ] );
		}
	}
}
