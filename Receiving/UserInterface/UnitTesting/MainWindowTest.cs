﻿#undef LOG
using System.Windows;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using UserInterface;
using NoteProcesser;

namespace UnitTesting {
    [TestClass]
    public class MainWindowTest {

        MainWindow m;

        [TestInitialize]
        public void TestInitialization ( ) {
            Logger.testing = true;
            m = new MainWindow ( );
        }

        #region Minor Test Cases
        [TestMethod]
        public void TestInitializePicture ( ) {
            MainWindow.InitializePictures ( );

            List<string> file = new List<string> ( ) {
                "whole", "half", "quarter", "eighth",
                "icon", "noteLine", "Measure"
            };

            Assert.AreEqual ( 7, MainWindow.PICTURELIST.Count );
            // If a file is not found, then the test failed
            foreach ( string s in new List<string> ( ) {
                "whole", "half", "quarter", "eighth",
                "icon", "noteLine", "Measure" } ) {
                MainWindow.PICTURELIST.Single ( z => z.UriSource.ToString ( )
                .Contains ( s ) );
            }
            // If we get here, all files were found and the test pass
        }

        [TestMethod]
        public void TestConstructor ( ) {
            Assert.AreEqual ( 1, m.measureList.Count );
            Assert.IsNotNull ( m.processer );

            if ( m.processer.isOpen ) {
                Assert.AreEqual ( State.CONNECT, m.state );
                Assert.IsNull ( m.reconnectThread );
            } else {
                Assert.AreEqual ( State.DISCONNECT, m.state );
                Assert.IsNotNull ( m.reconnectThread );
                Assert.IsTrue ( m.reconnectThread.IsAlive );
            }

            Assert.IsNotNull ( m.noteLineTimer );
            Assert.AreEqual ( "images/noteLine.png", m.noteLineImage.Source.ToString ( ) );
            Assert.IsTrue ( m.canvas.Children.Contains ( m.noteLineImage ) );
        }

        [TestMethod]
        public void TestErrorReceivedEvent ( ) {
            m.reconnectThread.Abort ( );
            m.ErrorReceived ( null );

            Assert.IsNotNull ( m.reconnectThread );
            Assert.IsTrue ( m.reconnectThread.IsAlive );
            Assert.AreEqual ( State.DISCONNECT, m.state );
        }

        [TestMethod]
        public void TestStartStopEvent ( ) {
            m.state = State.DISCONNECT;
            m.StartStop ( null, null );
            Assert.AreEqual ( State.DISCONNECT, m.state );

            m.state = State.CONNECT;
            m.StartStop ( null, null );
            Assert.AreEqual ( State.WAITING, m.state );

            m.state = State.PAUSE;
            m.StartStop ( null, null );
            Assert.AreEqual ( State.WAITING, m.state );

            m.state = State.WAITING;
            m.StartStop ( null, null );
            Assert.AreEqual ( State.PAUSE, m.state );

            m.state = State.RECORDING;
            m.StartStop ( null, null );
            Assert.AreEqual ( State.PAUSE, m.state );
        }

        [TestMethod]
        public void TestMoveNoteLine ( ) {
            m.state = State.RECORDING;
            for ( int i = 0; i < 3; i++ ) {
                Vector v = m.noteLinePos;
                m.MoveNoteLine ( null );
                Assert.IsTrue ( m.noteLinePos.X > v.X || m.noteLinePos.Y > v.Y );
            }
        }

        [TestMethod]
        public void TestMoveNoteLineWrap ( ) {
            m.state = State.RECORDING;
            m.noteLinePos = new Vector ( 970 - m.noteLineMove, 155 );
            Vector v = m.noteLinePos;

            m.MoveNoteLine ( null );
            Assert.AreEqual ( 970, m.noteLinePos.X );
            m.MoveNoteLine ( null );
            Assert.AreEqual ( 10 + m.noteLineMove, m.noteLinePos.X );
            Assert.AreEqual ( v.Y + 127, m.noteLinePos.Y );
        }

        [TestMethod]
        public void TestMoveNoteLineNewMeasure ( ) {
            m.state = State.RECORDING;
            m.noteLinePos = new Vector ( 970 - m.noteLineMove, 745 );
            int i = m.measureList.Count;

            m.MoveNoteLine ( null );
            Assert.AreEqual ( 970, m.noteLinePos.X );
            m.MoveNoteLine ( null );
            Assert.AreEqual ( 10 + m.noteLineMove, m.noteLinePos.X );
            Assert.AreEqual ( 110, m.noteLinePos.Y );
            Assert.AreEqual ( i + 1, m.measureList.Count );
        }
        #endregion Minor Test Cases

        #region Essential Test Cases
        /*
		 * Because we are adding and removing Notes, a new MainWindow must 
		 * be created everytime
		 */

        [TestMethod]
        public void TestDisplayNote_NotRecording ( ) {
            MainWindow m2 = new MainWindow ( );
            List<Measure> originalMeasureList = m2.measureList;
            m2.state = State.WAITING;
            m2.DisplayNote ( null, new List<(int, string)> ( ) { (1, "quarter") } );

            Assert.AreEqual ( State.RECORDING, m2.state );
            Assert.AreEqual ( 1, m2.measureList.Count );
        }

        [TestMethod]
        public void TestDisplayNote ( ) {
            MainWindow m2 = new MainWindow ( );
            List<Measure> originalMeasureList = m2.measureList;
            m2.state = State.WAITING;
            Vector pos = m2.noteLinePos;

            List<(int, string)> noteList = new List<(int, string)> ( );
            for ( int i = 0; i < 8; i++ )
                noteList.Add ( (i, "quarter") );
            m2.DisplayNote ( null, noteList );

            Assert.AreEqual ( State.RECORDING, m2.state );
            Assert.AreEqual ( 1, m2.measureList.Count );
            Assert.AreEqual ( 8, m2.measureList [ 0 ].size );

            for ( int i = 0; i < 8; i++ ) {
                double yPost = 0;
                Assert.AreEqual ( noteList [ i ].Item2, 
                    m2.measureList [ 0 ] [ i ].Item1.Name );
                switch ( i ) {
                    case 0:
                    case 1: yPost = pos.Y - 22 + 4.5; break;    // For Ride + Crash
                    case 2: yPost = pos.Y - 22 + 8.5; break;      // For Hi-hat
                    case 3: yPost = pos.Y - 22 + 13.5; break;   // For High-Tom
                    case 4: yPost = pos.Y - 22 + 18.5; break;   // For Middle-tom
                    case 5: yPost = pos.Y - 22 + 23.5; break;     // For Snare
                    case 6: yPost = pos.Y - 22 + 32.5; break;   // For Floor-tom
                    case 7: yPost = pos.Y - 22 + 42.5; break;   // For Bass Drum
                }
                Assert.AreEqual ( new Vector ( pos.X - m2.noteLineMove, yPost ), 
                    m2.measureList [ 0 ] [ i ].Item2 );
            }
        }

        [TestMethod]
        public void TestDisplayNote_MoveLine ( ) {
            MainWindow m2 = new MainWindow ( );
            List<Measure> originalMeasureList = m2.measureList;
            m2.state = State.WAITING;

            (int, string) n = (1, "quarter");
            (int, string) n2 = (2, "whole");
            (int, string) n3 = (3, "half");

            Vector pos = m2.noteLinePos;

            m2.DisplayNote ( null, new List<(int, string)> ( ) { n } );
            Assert.AreEqual ( new Vector ( pos.X - m2.noteLineMove, pos.Y - 22 + 4.5 ),
                m2.measureList [ 0 ] [ 0 ].Item2 );
            Assert.AreEqual ( "quarter", m2.measureList [ 0 ] [ 0 ].Item1.Name );

            m2.MoveNoteLine ( null );
            pos = m2.noteLinePos;
            m2.DisplayNote ( null, new List<(int, string)> ( ) { n2 } );
            Assert.AreEqual ( new Vector ( pos.X - m2.noteLineMove, pos.Y - 22 + 8.5 ),
                m2.measureList [ 0 ] [ 1 ].Item2 );
            Assert.AreEqual ( "whole", m2.measureList [ 0 ] [ 1 ].Item1.Name );

            m2.MoveNoteLine ( null );
            pos = m2.noteLinePos;
            m2.DisplayNote ( null, new List<(int, string)> ( ) { n3 } );
            Assert.AreEqual ( new Vector ( pos.X - m2.noteLineMove, pos.Y - 22 + 13.5 ),
                m2.measureList [ 0 ] [ 2 ].Item2 );
            Assert.AreEqual ( "half", m2.measureList [ 0 ] [ 2 ].Item1.Name );

            Assert.AreEqual ( 1, m2.measureList.Count );
            Assert.AreEqual ( 3, m2.measureList [ 0 ].size );
        }

        [TestMethod]
        public void TestDisplayNote_NewMeasure ( ) {
            MainWindow m2 = new MainWindow ( );
            List<Measure> originalMeasureList = m2.measureList;
            m2.state = State.WAITING;

            (int, string) n =  ( 1, "quarter" );
            (int, string) n2 =  ( 2, "whole" );

            m2.noteLinePos = new Vector ( 970, 745 );

            Vector pos = m2.noteLinePos;
            m2.DisplayNote ( null, new List<(int, string)> ( ) { n } );
            Assert.AreEqual ( new Vector ( pos.X - m2.noteLineMove, pos.Y - 22 + 4.5 ), 
                m2.measureList [ 0 ] [ 0 ].Item2 );
            Assert.AreEqual ( "quarter", m2.measureList [ 0 ] [ 0 ].Item1.Name );

            m2.MoveNoteLine ( null );
            pos = m2.noteLinePos;
            m2.DisplayNote ( null, new List<(int, string)> ( ) { n2 } );
            Assert.AreEqual ( new Vector ( pos.X - m2.noteLineMove, pos.Y - 22 + 8.5 ), 
                m2.measureList [ 1 ] [ 0 ].Item2 );
            Assert.AreEqual ( "whole", m2.measureList [ 1 ] [ 0 ].Item1.Name );

            Assert.AreEqual ( 2, m2.measureList.Count );
            Assert.AreEqual ( 1, m2.measureList [ 0 ].size );
            Assert.AreEqual ( 1, m2.measureList [ 1 ].size );
        }
        #endregion Essential Test Cases
    }
}