﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using NoteProcesser;
using System.Windows;
using System.Windows.Controls;
using UserInterface;

namespace UnitTesting {
    [TestClass]
    public class SaveLoadTests {

        [TestMethod]
        public void TestSaveLoad ( ) {
            MainWindow.InitializePictures ( );
            List<Measure> measure = new List<Measure> ( ) { new Measure ( ) };
            List<Measure> loadMeasure = new List<Measure> ( );
            measure [ 0 ].AddNote (
                ( new Image ( ) { Name = "quarter"}, new Vector ( 10, 25 )) );
            measure [ 0 ].AddNote (
                ( new Image ( ) { Name = "half"}, new Vector ( 10, 55 )) );
            measure [ 0 ].AddNote (
                ( new Image ( ) { Name = "whole"}, new Vector ( 52, 720 )) );

            if ( !MainWindow.SaveSession ( @"./TestSave.drum", measure ) ) Assert.Fail ( );
            if ( !MainWindow.LoadSession ( @"./TestSave.drum", ref loadMeasure ) ) Assert.Fail ( );

            Assert.AreEqual ( 1, loadMeasure.Count );
            Assert.AreEqual ( 3, loadMeasure[0].size );
            for (int i = 0; i < loadMeasure[0].size; i++ ) {
                Assert.AreEqual ( measure [ 0 ] [ i ].Item1.Name, 
                    loadMeasure [ 0 ] [ i ].Item1.Name );
                Assert.AreEqual ( measure [ 0 ] [ i ].Item2, loadMeasure [ 0 ] [ i ].Item2 );
            }
        }

        [TestMethod]
        public void TestSaveLoad_NewMeasure ( ) {
            MainWindow.InitializePictures ( );
            List<Measure> measure = new List<Measure> ( ) { new Measure ( ) };
            measure [ 0 ].AddNote (
                ( new Image ( ) { Name = "quarter"}, new Vector ( 10, 25 )) );
            measure [ 0 ].AddNote (
                ( new Image ( ) { Name = "half"}, new Vector ( 10, 55 )) );
            measure [ 0 ].AddNote (
                ( new Image ( ) { Name = "whole"}, new Vector ( 52, 720 )) );

            measure.Add ( new Measure ( ) );
            measure [ 1 ].AddNote (
                ( new Image ( ) { Name = "quarter"}, new Vector ( 30, 99 )) );
            measure [ 1 ].AddNote (
                ( new Image ( ) { Name = "half"}, new Vector ( 30, 10 )) );
            measure [ 1 ].AddNote (
                ( new Image ( ) { Name = "whole"}, new Vector ( 100, 999 )) );
            measure [ 1 ].AddNote (
                ( new Image ( ) { Name = "eighth"}, new Vector ( 300, 666 )) );

            List<Measure> loadMeasure = new List<Measure> ( );
            if ( !MainWindow.SaveSession ( @"./TestSave.drum", measure ) ) Assert.Fail ( );
            if ( !MainWindow.LoadSession ( @"./TestSave.drum", ref loadMeasure ) ) Assert.Fail ( );

            Assert.AreEqual ( 2, loadMeasure.Count );
            Assert.AreEqual ( 3, loadMeasure[0].size );
            Assert.AreEqual ( 4, loadMeasure[1].size );
            for (int i = 0; i < loadMeasure[0].size; i++ ) {
                Assert.AreEqual ( measure [ 0 ] [ i ].Item1.Name, 
                    loadMeasure [ 0 ] [ i ].Item1.Name );
                Assert.AreEqual ( measure [ 0 ] [ i ].Item2, 
                    loadMeasure [ 0 ] [ i ].Item2 );
            }
            for ( int i = 0; i < loadMeasure [ 1 ].size; i++ ) {
                Assert.AreEqual ( measure [ 1 ] [ i ].Item1.Name,
                    loadMeasure [ 1 ] [ i ].Item1.Name );
                Assert.AreEqual ( measure [ 1 ] [ i ].Item2, 
                    loadMeasure [ 1 ] [ i ].Item2 );
            }
        }
    }
}
