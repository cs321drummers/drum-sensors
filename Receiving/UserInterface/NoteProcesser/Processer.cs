﻿#undef DEBUG
#define LOG
#undef LOG2
#undef LOG
#if !LOG
#undef LOG2
#endif
#define HACK
#undef HACK

using System;
using System.Text;
using System.IO.Ports;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

namespace NoteProcesser {
    public delegate void ProcessedNoteEvent ( object sender, List<(int, string)> e );
    public delegate void ErrorReceivedEvent ( object sender );
#if LOG
    public delegate void Logging ( string text );
#endif

    public class Processer : IDisposable {

        [System.Diagnostics.CodeAnalysis.SuppressMessage ( "Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly" )]
        public event ProcessedNoteEvent processedNote;
        [System.Diagnostics.CodeAnalysis.SuppressMessage ( "Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly" )]
        public event ErrorReceivedEvent errorReceived;
#if LOG
        public static event Logging Log;
#endif

        /// <summary>
        /// This class will attempt to connect to a serial port and read data from the port
        /// </summary>
        private SerialPort port;
#if !HACK
        public bool isOpen => ( bool ) port?.IsOpen;
#else
        public bool isOpen { get; set; }
#endif

        /// <summary>
        /// Get all the available ports
        /// </summary>
        /// <returns>A list of string containing all open ports</returns>
        public static List<string> GetListofSerial ( ) => SerialPort.GetPortNames ( ).ToList ( );

        /// <summary>
        /// Create a serial port using the name of the port as reference
        /// </summary>
        /// <param name="errorReceived">A callback method if there is an error sending and receiving data</param>
        /// <param name="portName">The port to connect to, leave blank if wish to connect automatically</param>
        public Processer ( string portName = "" ) {
#if LOG
            Log?.Invoke ( "Initializing Processer" );
            Log?.Invoke ( "\tPort Name Para is -> " + portName );
#endif
#if !HACK
            // Ensure that the name is legal or if null, try to find Arduion automatically
            if ( portName == "" || portName.ToLower ( ).StartsWith ( "com" ) ) {
#if LOG
                Log?.Invoke ( "\tPort Name valid" );
                Log?.Invoke ( "\tCreating Port..." );
#endif
                port = new SerialPort ( ) { BaudRate = 9600 };
                FindArduino ( portName );

                port.ErrorReceived += PortErrorReceive;
#if LOG
                Log?.Invoke ( "\tErrorReceived Event Hooked" );
                Log?.Invoke ( "\tCurrent Status of Port is " + port.IsOpen );
                Log?.Invoke ( "\tExiting Processer" );
#endif
                return;
            }
#if LOG
            Log?.Invoke ( "\tInvalid Parameter" );
#endif
            throw new ArgumentException ( "Invalid Com Port" );
#else
            Thread hack = new Thread ( Hack );
            hack.Start ( );
#endif
        }

#if HACK
        public void Hack ( ) {
            isOpen = true;
            int num = 1;
            while ( num < 1000 ) {
                byte [ ] b = new byte [ 8 ];
                b [ 0 ] = num % 10 == 0 ? ( byte ) 1 : ( byte ) 0;
                b [ 1 ] = num % 2 == 0 ? ( byte ) 1 : ( byte ) 0;
                b [ 2 ] = num % 3 == 0 ? ( byte ) 1 : ( byte ) 0;
                b [ 3 ] = num % 4 == 0 ? ( byte ) 1 : ( byte ) 0;
                b [ 4 ] = num % 5 == 0 ? ( byte ) 1 : ( byte ) 0;
                b [ 5 ] = num % 6 == 0 ? ( byte ) 1 : ( byte ) 0;
                b [ 6 ] = num % 7 == 0 ? ( byte ) 1 : ( byte ) 0;
                b [ 7 ] = num % 8 == 0 ? ( byte ) 1 : ( byte ) 0;

                ProcessData ( b );
                num++;
                Thread.Sleep ( 250 );
            }
        }
#endif

        /// <summary>
        /// Try to find the port the Arduino is in
        /// </summary>
        /// <param name="com">Pass a valid Com Port to attempt connection, else leave blank</param>
        /// <returns>True if Arduion was found. False if there are no port or no Arduino was found</returns>
        public bool FindArduino ( string com = "" ) {
#if LOG
            Log?.Invoke ( "\tStarting FindArduino with " + com );
#endif
            List<string> comList;
            if ( !string.IsNullOrEmpty ( com ) ) {
#if LOG
                Log?.Invoke ( "\t\tUsing Preset ComPort" );
#endif
                comList = new List<string> ( ) { com };
            } else {
#if LOG
                Log?.Invoke ( "\t\tGetting List of Ports..." );
#endif
                comList = GetListofSerial ( );
            }

            port.Close ( );

            try {
                // go through each open com
                foreach ( string s in comList ) {
#if LOG
                    Log?.Invoke ( "\t\tEstablishing connection with Port -> " + s );
#endif
                    // connect to the com port
                    port.PortName = s;
                    port.Open ( );
                    port.DiscardInBuffer ( );
                    port.DiscardOutBuffer ( );
#if LOG
                    Log?.Invoke ( "\t\tPort Open, Buffer Discarded, About to go to sleep" );
#endif
                    System.Threading.Thread.Sleep ( 1000 );
                    try {
                        // TODO
                        // try port.ReadLine instead??
                        string dataString = port.ReadLine ( );

                        byte [ ] data = Encoding.Default.GetBytes ( dataString );
                        //port.Read ( data, 0, 5 );
#if LOG
                        Log?.Invoke ( "\t\tData Read..." );
                        string byteData = "";
                        foreach ( byte b in data )
                            byteData += b + " | ";
                        Log?.Invoke ( "\t\tByte Received -> " + byteData );
#endif
                        if ( data [ 0 ] == 64 && data [ 1 ] == 114 && data [ 2 ] == 100 ) {
#if LOG
                            Log?.Invoke ( "\t\tConnection Accept... sending \"C#\" to Arduino" );
#endif
                            port.WriteLine ( "C#" );
                            port.DiscardInBuffer ( );
                            port.DataReceived += new SerialDataReceivedEventHandler ( DataReceivedEvent );
#if LOG
                            Log?.Invoke ( "\t\tSingal sent, Buffer Cleared, DataReceive Event Hooked, Exiting FindArduino" );
#endif
                            return true;
                        }
                    } catch ( Exception e ) {
#if LOG
                        Log?.Invoke ( "\t\tERROR: Port Read Fail" );
                        Log?.Invoke ( "\t\tERROR: Exception ->" + e );
#endif
                    }

#if LOG
                    Log?.Invoke ( "\t\tInvalid data, skipping port..." );
#endif
                    // else we just close and try the next port
                    port.Close ( );
                }
            } catch ( Exception e ) {

#if LOG
                Log?.Invoke ( "\t\tERROR: Open Port Fail" );
                Log?.Invoke ( "\t\tERROR: Exception -> " + e );
#endif
            }

#if LOG
            Log?.Invoke ( "\t\tNo valid port signal found... Exiting FindArduino" );
#endif
            return false;
        }

        /// <summary>
        /// Send a byte array to the serial port
        /// </summary>
        /// <param name="bytearray"></param>
        public void SendData ( byte [ ] bytearray ) {
#if LOG
            Log?.Invoke ( "\tStarting SendData..." );
            Log?.Invoke ( "\tDataArray to send is ->" );
            string data = "";
            foreach ( byte b in bytearray )
                data += b + " | ";
            Log?.Invoke ( "\t\t\t" + data );
#endif
            port.Write ( bytearray, 0, 0 );
#if LOG
            Log?.Invoke ( "\tExiting SendData..." );
#endif
        }

        /// <summary>
        /// Called when the serial ported received a package. Attempt to read all the data
        /// and alert the main program
        private void DataReceivedEvent ( object sender, SerialDataReceivedEventArgs e ) {
#if LOG
            Log?.Invoke ( "\tData from Arduino received" );
#endif
            try {
                string data = port.ReadLine ( );
                byte [ ] dataArray = Encoding.Default.GetBytes ( data );
#if LOG2
                string byteData = "";
                foreach ( byte b in dataArray )
                    byteData += b + " | ";
                Log?.Invoke ( "\t\tByte Received -> " + byteData );
#endif
                if ( dataArray.ToList ( ).Contains ( 1 ) ) { // only send data if there is a 1
#if LOG
                    string byteData = "";
                    foreach ( byte b in dataArray )
                        byteData += b + " | ";
                    Log?.Invoke ( "\t\tByte Received -> " + byteData );
#endif
                    ProcessData ( dataArray );
                }
            } catch ( Exception a ) {
#if LOG
                Log?.Invoke ( "\tERROR: Unable to read data package" );
                Log?.Invoke ( "\tERROR: Exception ->" + a );
#endif
                errorReceived?.Invoke ( this );
            }
        }

        /// <summary>
        /// Process the data received
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProcessData ( byte [ ] e ) {
#if LOG
            Log?.Invoke ( "\tStarting ProcessData..." );
#endif
            List<(int, string)> noteList = new List<(int, string)> ( );
            for ( int i = 0; i < e.Length; i++ ) {
#if LOG
                Log?.Invoke ( "\t\t - Note #" + i + " Value: " + e [ i ] );
#endif
                if ( e [ i ] == 1 ) {
#if LOG
                    Log?.Invoke ( "\t\t\t - Note added to list" );
#endif
                    noteList.Add ( (i, "quarter") );
                }
            }
#if LOG
            Log?.Invoke ( "\t\t Sending over noteList" );
            Log?.Invoke ( "\t\t noteList Contains" );
            string note = "";
            foreach ( Note n in noteList ) {
                note += n.ToString ( );
            }
            Log?.Invoke ( "\t\t\t - " + note );
            Log?.Invoke ( "\t\tSending noteList to MainWindow, Exiting ProcessData..." );
#endif
            processedNote?.Invoke ( this, noteList );
        }

        internal void PortErrorReceive ( object sender, SerialErrorReceivedEventArgs e ) => errorReceived?.Invoke ( this );

        /// <summary>
        /// Free resources and remove port object
        /// </summary>
        /// <returns></returns>
        public void Dispose ( ) {
            Dispose ( true );
            GC.SuppressFinalize ( this );
        }

        protected virtual void Dispose ( bool dispose ) {
#if LOG
            Log?.Invoke ( "\tDisposing Processer..." );
#endif
            if ( dispose ) {
                if ( port.IsOpen ) {
#if LOG
                    Log?.Invoke ( "\t\tPort is Open" );
                    Log ( "\tPort is open... Sending exit code of 51, 120, 105" );
#endif

                    port.DiscardInBuffer ( );
                    port.DiscardOutBuffer ( );
                    port.WriteLine ( "3xi" );
                    //port.DataReceived -= DataReceivedEvent;
                    //Thread.Sleep(1000);
                    //while (true){}


#if LOG
                    Log?.Invoke ( "\t\tPort's Buffer In and Out is Clear" );
#endif
                    port.Close ( );
                    port.Dispose ( );
#if LOG
                    Log?.Invoke ( "\t\tPort is closed and disposed" );
#endif
                }
            }
#if LOG
            Log?.Invoke ( "\tFinish disposing Processer" );
#endif
        }
    }
}