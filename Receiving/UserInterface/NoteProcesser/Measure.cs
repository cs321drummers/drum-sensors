﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows;

namespace NoteProcesser {
	public class Measure {
		public (Image, Vector) this [ int i ] => imageList [ i ];
        [System.Diagnostics.CodeAnalysis.SuppressMessage ( "Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations" )]
        public (Image, Vector) this [ string i ] {
			get {
				switch ( i.ToLower ( ) ) {
					case "first":
						return this [ 0 ];
					case "last":
						return this [ size - 1 ];
					default:
						throw new IndexOutOfRangeException ( "Only First and Last are valid inputs" );
				}
			}
		}

		public int size => imageList.Count;
		private List<(Image, Vector)> imageList;

		/// <summary>
		/// Create a measure
		/// </summary>
		public Measure ( ) => imageList = new List<(Image, Vector)> ( );

		/// <summary>
		/// Add a note to the measure. This will record and save the note data and information
		/// </summary>
		/// <param name="note">The Image of the note and the position of the note as a tuple</param>
		public void AddNote ( (Image, Vector) note ) => imageList.Add ( note);
	}
}
